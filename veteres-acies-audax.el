;; veteres acies audax v0.1
;; Hint - C-u arg M-x function RET

;; duplication of region
(defun duplication (&optional arg)
  (interactive "*p")
    (if (or (string= last-command "yank")
     (string= last-command "yank-pop"))
      (yank-pop arg)
      (if (region-active-p)
       (kill-ring-save (region-beginning) (region-end))
    	(kill-ring-save (line-beginning-position) (line-beginning-position 2)))
      (if (> arg 1)
       (dotimes 'arg (newline-and-indent)(yank))
       (message "Previous arg was not a yank, and called without a prefix."))))

;; remove lines
(defun inquisition (&optional arg)
  (interactive "*p")
  (dotimes 'arg (kill-whole-line)))
