Emacs region/lines functions

* **duplication**(N) - clone selected region N times
* **inquisition**(N) - remove N lines